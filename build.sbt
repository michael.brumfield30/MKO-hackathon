name := "KafkaConsumer"

version := "1.0"

scalaVersion := "2.11.11"

resolvers += "Cloudera Repo" at "https://repository.cloudera.com/artifactory/cloudera-repos/"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core"  % "2.1.0.cloudera1" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.1.0.cloudera1" % "provided",
  "org.apache.kafka" %% "kafka" % "0.9.0.0",
  "org.apache.kafka" % "kafka-clients" % "0.9.0.0",
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "org.json4s" %% "json4s-native" % "3.2.11",
  "org.json4s" %% "json4s-ast" % "3.2.11",
  "com.typesafe" % "config" % "1.3.1",
  "com.iheart" %% "ficus" % "1.4.1",
  "com.generalmills" % "cdf_data_event_processor_2.11" % "1.0.1",
  "org.apache.kudu" % "kudu-spark2_2.11" % "1.3.0-cdh5.11.0_patch2295",
  "com.generalmills" % "scala-core_2.11" % "1.0.0"
)

// META-INF discarding
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case "application.conf" => MergeStrategy.concat
  case "reference.conf" => MergeStrategy.concat
  case x => MergeStrategy.first
}
    