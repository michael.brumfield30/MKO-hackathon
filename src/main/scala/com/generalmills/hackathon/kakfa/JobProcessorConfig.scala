package com.generalmills.hackathon.kakfa

import com.generalmills.datalake.jobprocessor.entities.AppConfig
import com.generalmills.environment._
import com.typesafe.config._
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
/**
  * Created by G554027 on 3/23/2018.
  */
object JobProcessorConfig {
  private val _config: Config = ConfigFactory.load()
  def apply(): AppConfig = {
    _config.as[AppConfig]("com.generalmills.hackathon.kakfa")
  }
}
