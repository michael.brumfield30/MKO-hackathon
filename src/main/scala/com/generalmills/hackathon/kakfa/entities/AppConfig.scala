package com.generalmills.hackathon.kakfa.entities

/**
  * Created by G554027 on 3/23/2018.
  */
case class AppConfig(kafkaPollTimeOutInMs: Long,
                     maxNoOfPollsWithoutRecords: Int,
                     kafkaBrokers: String)

