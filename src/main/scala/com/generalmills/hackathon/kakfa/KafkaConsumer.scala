package com.generalmills.hackathon.kakfa

import java.io.StringWriter
import java.util

import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import com.generalmills.environment._

import scala.util.control.Breaks._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._
import java.util.Properties

import com.generalmills.datalake.jobprocessor.entities.TableEventMessage
import com.generalmills.datalake.jobprocessor.util.Logger


class KafkaConsumerUtil {
  implicit val formats = DefaultFormats
  private var kafkaConsumer: KafkaConsumer[String, String] = _

  def getKafkaConsumer(topicName: String, consumerGroupId: String): KafkaConsumer[String, String] = {
    val properties = new Properties()
    properties.put("bootstrap.servers", JobProcessorConfig().kafkaBrokers)
    properties.put("group.id", consumerGroupId)
    properties.put("enable.auto.commit", "false")
    properties.put("auto.offset.reset", "earliest")
    properties.put("key.deserializer", classOf[StringDeserializer])
    properties.put("value.deserializer", classOf[StringDeserializer])
    val kafkaConsumer = new KafkaConsumer[String, String](properties)

    val topiclist: util.List[String] = new util.ArrayList[String]()
    topiclist.add(topicName)
    kafkaConsumer.subscribe(topiclist)
    //kafkaConsumer.subscribe(List(topicName).asJavaCollection)
    kafkaConsumer
  }

  def getEvents: List[TableEventMessage] = {
    val buffer = new ListBuffer[List[ConsumerRecord[String, String]]]
    val maxNoOfPollsWithoutRecords = JobProcessorConfig().maxNoOfPollsWithoutRecords
    var counter = 0

    breakable {
      while (true) {
        val records = kafkaConsumer.poll(JobProcessorConfig().kafkaPollTimeOutInMs).asScala.toList
        if (records.isEmpty) {
          counter = counter + 1
          Logger.log.info(s"Kafka polling empty records returned counter: $counter")
        }
        else {
          buffer += records
          Logger.log.info(s"Kafka polling count of records returned: ${records.length}")
        }
        if (counter > maxNoOfPollsWithoutRecords) {
          Logger.log.info(s"Breaking...Kafka polling end of empty records counter reached: $counter")
          break
        }
      }
    }

    val tableEventMessages =
      buffer.toList.flatten.map(toTableEventMessage => {
        try {
          Logger.log.info("Parsing Kafka Message...")
          Logger.log.info(s"   Partition: ${toTableEventMessage.partition()}")
          Logger.log.info(s"   Offset: ${toTableEventMessage.offset()}")
          parse(toTableEventMessage.value()).extract[TableEventMessage]
        }
        catch {
          case exception: Exception => {
            val sw = new StringWriter
            Logger.log.error(s"Error in Parsing Kafka Message...")
            Logger.log.error(s"   Kafka Message: $toTableEventMessage")
            Logger.log.error(s"   Error Message: ${exception.getMessage}")
            Logger.log.error(s"   Error Stack Trace: ${sw.toString}")
            TableEventMessage("", isTableFullReload = false, "", "", None)
          }
        }
      })

    Logger.log.info(s"Total number of kafka messages: ${tableEventMessages.length}")
    tableEventMessages
  }

  def close(): Unit = {
    try {
      kafkaConsumer.close()
    }
    catch {
      case illegalStateException: IllegalStateException => // Do not do anything
      case otherException: Any => throw otherException
    }
  }

  def commitOffsets(): Unit = {
    kafkaConsumer.commitSync()
  }

  def commitAndClose() : Unit = {
    commitOffsets()
    close()
  }
}

object KafkaConsumerUtil {
  def apply(topicName: String, consumerGroupId: String): KafkaConsumerUtil = {
    val kafkaUtil = new KafkaConsumerUtil
    kafkaUtil.kafkaConsumer = kafkaUtil.getKafkaConsumer(topicName, consumerGroupId)
    kafkaUtil
  }
}

